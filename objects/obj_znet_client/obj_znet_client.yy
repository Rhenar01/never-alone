{
    "id": "594574a0-79c0-4768-9f73-d022b6bb3dd6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_znet_client",
    "eventList": [
        {
            "id": "6036e6ae-4344-4e1e-8039-89729e540358",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "594574a0-79c0-4768-9f73-d022b6bb3dd6"
        },
        {
            "id": "1e3a25d0-416d-4f9c-b5d3-4526ea6f6bdb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "594574a0-79c0-4768-9f73-d022b6bb3dd6"
        },
        {
            "id": "db993765-35d7-4362-b586-dd2ca4927b00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "594574a0-79c0-4768-9f73-d022b6bb3dd6"
        },
        {
            "id": "d2f79896-2977-450c-9e09-397d3c37741b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "594574a0-79c0-4768-9f73-d022b6bb3dd6"
        },
        {
            "id": "46f9a5ea-5729-4aec-8dc8-b06bfe5452c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "594574a0-79c0-4768-9f73-d022b6bb3dd6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}