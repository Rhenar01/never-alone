{
    "id": "8e32dbb5-74d9-4336-853a-7d2d48c20d0b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_znet_server",
    "eventList": [
        {
            "id": "4520f8e0-55e3-4d8a-a726-a87fd5c5d726",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e32dbb5-74d9-4336-853a-7d2d48c20d0b"
        },
        {
            "id": "8b545df3-0be9-4527-89e0-4cacd5ff9941",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8e32dbb5-74d9-4336-853a-7d2d48c20d0b"
        },
        {
            "id": "bf33fe02-555d-4721-9954-b693ec5887bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "8e32dbb5-74d9-4336-853a-7d2d48c20d0b"
        },
        {
            "id": "cb14c4ac-f6a0-48fc-8b75-3e55d29a8d90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "8e32dbb5-74d9-4336-853a-7d2d48c20d0b"
        },
        {
            "id": "6b010bc1-2169-4346-b668-2f44009a551c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "8e32dbb5-74d9-4336-853a-7d2d48c20d0b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}